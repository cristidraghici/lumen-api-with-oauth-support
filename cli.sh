#!/bin/bash

case "$OSTYPE" in
  darwin*)  OS="darwin" ;;
  linux*)   OS="linux" ;;
  *)        echo "unknown: $OSTYPE"; exit ;;
esac

function usage {
  echo ""
  echo "The parameters to use with this tool are the following: "
  echo ""
  echo "  start"
  echo "  stop"
  echo "  build"
  echo "  exec <command> - execute command in lao-lummen"
  echo ""
  echo "e.g. ./cli.sh exec bash"
  echo "     ./cli.sh exec composer install"
  echo ""
}

if [ $# -eq 0 ]; then
    usage;
    exit 1
fi

[ ! "$(docker network list | grep lao-network)" ] && docker network create lao-network


case $1 in
  build)
    (cd docker && docker-compose down && docker-compose -f ./docker-compose.yml up -d --build --force-recreate)
    ;;
  start)
    (cd docker && docker-compose -f ./docker-compose.yml up -d)
    ;;
  stop)
    (cd docker && docker-compose down)
    ;;
  exec)
    shift;
    docker exec -it lao-lumen $@;
    ;;
  *)
    usage;
    ;;
esac
