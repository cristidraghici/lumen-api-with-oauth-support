# Lumen API with OAuth for Google Authentication

> An project example

## CLI

Running the `./cli.sh` file might previously need: `chomd +x ./cli.sh`

## Read more

[Creating a Lumen API with OAuth for Google Authentication](https://draghici.net/) on draghici.net.
