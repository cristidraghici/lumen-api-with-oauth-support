<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return 'Hello, there! :)';
});

$router->get('/api', function () use ($router) {
    return [
        'data' => $router->app->version(),
        'status' => true
    ];
});

$router->post('/api/v1/register','UsersController@register');
